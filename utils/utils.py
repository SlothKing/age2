import requests
import pandas as pd
import os
import numpy as np
import plotly.graph_objects as go
from django.conf import settings
from base.models import Leaderboard
from django.utils import timezone as tz
from bs4 import BeautifulSoup as bs
from django.shortcuts import get_object_or_404


def harvester_de(base_url, id):
    print('Leaderboard: {}'.format(settings.TITLES_DE[id]))
    name = '{date}_de_{leaderboard_id}.txt'.format(date=tz.now().strftime('%Y-%m-%d'),
                                    leaderboard_id=id)
    path = settings.STATIC_ROOT + '/csv_leaderboards/' + name

    query =  Leaderboard.objects.filter(csv_table__contains=path)
    if query.exists():
        return get_object_or_404(query)

    window_index = 1
    step = 10000
    data = pd.DataFrame()
    print('Parameters set. Starting the harvesting')

    while True:
        print('Collecting entries from {} to {}'.format(window_index, window_index + step -1))
        request  = base_url.format(leaderboard_id=id, start=window_index, count=step)
        raw = requests.get(request).json()
        print('Request handeled. Converting to pandas DataFrame.')
        aux = pd.DataFrame(raw['leaderboard'])
        print('Conversion completed. Merging the dataset')
        if not aux.empty:
            window_index = window_index + step
            data = data.append(aux, ignore_index=True, sort=False)
            print('Process completed. Starting the analysis of next batch...')
        else:
            print('Data harvested!')
            break
    print('Zipping the dataset...')
    data.to_csv(path)
    print('Process completed!')

    result = Leaderboard.objects.create(leaderboard_id=id,
                         date=tz.now(),
                         csv_table=path,
                         population=len(data.index),
                         average_elo=data.rating.mean(),
                         top_player=data.name.iat[0],
                         game='aoe2de'
                         )
    return result


def plot(leaderboard):
    print('Check if plot already exisits')
    if leaderboard.plot is not None:
        return leaderboard.plot

    name = '{date}_de_{leaderboard_id}.txt'.format(date=tz.now().strftime('%Y-%m-%d'),
                                    leaderboard_id=leaderboard.id)
    path =  settings.STATIC_ROOT + '/html_plots/' + name
    df = pd.read_csv(leaderboard.csv_table)
    print("Leaderboard population is: {pop}".format(pop=leaderboard.population))
    percentage = [(1 - (sum(df.rating < x) + 0.5*sum(df.rating == x ))/leaderboard.population)*100 for x in df.rating]
    print('Start Plotting')
    fig = go.Figure()
    print('Figure initiated')
    fig.add_trace(go.Histogram(x=df.rating, hoverinfo = 'none'))
    fig.add_trace(go.Scatter(x=df.rating,
            y=percentage,
            hovertemplate='Elo: %{x:f}<br>' + 'In the top %{y:f}%<extra></extra>',
            mode="lines",
            opacity = 0
    ))
    print('Updating layout')
    fig.update_layout(barmode='overlay')
    fig.update_layout(hovermode='x unified')
    fig.update_layout(showlegend=False)
    fig.write_html(path, include_plotlyjs='cdn')
    print('Cleaning the output')
    with open(path, 'r') as f:
        soup = bs(f, 'html.parser')


    leaderboard.plot = str(soup.find('div'))
    leaderboard.save()
