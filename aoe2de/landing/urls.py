from django.urls import path, include

from . import views

app_name = 'landing'

urlpatterns = [
    path('', views.index, name='index'),
    path('leaderboards/', include('leaderboards.urls') ),
    path('about', views.about, name='about'),
    path('resources', views.resources, name='resources'),
    path('contacts', views.contacts, name='contacts')
]
