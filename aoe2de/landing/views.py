from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'landing/index.html', {'nbar': 'home'})

def about(request):
    return render(request, 'landing/about.html', {'nbar': 'about'})

def resources(request):
    return render(request, 'landing/resources.html', {'nbar': 'resources'})

def contacts(request):
    return render(request, 'landing/contact.html', {'nbar': 'contacts'})
