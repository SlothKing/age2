from django.shortcuts import render, get_object_or_404
from django.conf import settings
from base.models import Leaderboard
from django.utils import timezone as tz

def index(request):
    return render(request, 'leaderboards/index.html',
                    context={
                    'titles_de': settings.TITLES_DE,
                    'titles_hd': settings.TITLES_HD,
                    'nbar': 'leaderboards'
                    })
def results(request, identifier):
    if identifier.isdigit():
        title = settings.TITLES_DE[int(identifier)]
    else:
        title = settings.TITLES_HD[identifier]
        context = {
                    'title' : title,
                    'nbar': 'leaderboards'
                    }
        return render(request, 'leaderboards/placeholder.html', context = context)


    name = name = '{date}_de_{leaderboard_id}.txt'.format(date=tz.now().strftime('%Y-%m-%d'),
                                    leaderboard_id=identifier)
    query =  Leaderboard.objects.filter(csv_table__contains=name)

    leaderboard = get_object_or_404(query)

    context = {
                'leaderboard' : leaderboard,
                'title' : title,
                'nbar': 'leaderboards'
                }

    return render(request, 'leaderboards/results.html', context = context)
