from django.core.management.base import BaseCommand, CommandError
import utils.utils as utl
from base.models import Leaderboard
from  django.conf import settings

class Command(BaseCommand):
    help = 'Utility to harvest leaderboards from www.aoe2.net'
    def add_arguments(self, parser):

        # Named (optional) arguments
        parser.add_argument(
            '--single',
            help='Update just a single leaderboard',
        )


    def handle(self, *args, **options):
        if options['single']:
            leaderboard_id = options['single']
            if leaderboard_id.isdigit():
                id = int(leaderboard_id)
                try:
                    settings.TITLES_DE[id]
                except KeyError:
                    self.stderr.write('The value is not a valid leaderboard id')
                    raise
                base_url = 'https://aoe2.net/api/leaderboard?game=aoe2de&leaderboard_id={leaderboard_id}&start={start}&count={count}'
                result = utl.harvester_de(base_url, id)
                dump = utl.plot(result)

            else:
                self.stdout.write('Not yet implemented: HD Leaderboard')
        else:
            for leaderboard_id in settings.TITLES_DE.keys():
                 base_url = 'https://aoe2.net/api/leaderboard?game=aoe2de&leaderboard_id={leaderboard_id}&start={start}&count={count}'
                 result = utl.harvester_de(base_url, leaderboard_id)
                 dump = utl.plot(result)

            for leaderboard_id in settings.TITLES_HD.keys():
                self.stdout.write('Not yet implemented: HD Leaderboard')
