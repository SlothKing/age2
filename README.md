# Age of Empires 2: Data Edition

This is a small project for me to learn Django.

The objective is to create a website to analyze data from [AoE2.net](www.aoe2.net).

It uses `Django` as framework, `pandas` for the data manipulation and `plotly` for the visualization.

## TODO List

Here it is a list of features I need to implement (in no particular order)

### Mandatory
- [x] API interaction
- [x] Management Utilities
- [x] Interactive Plotting
- [ ] Filling Placeholder
	- [x] Landing page
	- [ ] About section
	- [ ] Footer


- [ ] Heroku Deployment
- [ ] Admin page
	- [ ] `manage.py` commands from admin panel
- [ ] CSS Styling
	- [ ] Fix mobile navbar
	- [ ] Landing page
	- [x] Sticky footer
	- [ ] Palette
- [ ] Match predictions
- [ ] Voobly leaderboards

### Maybes
- [ ] Player Database
- [ ] Player Profiles
- [ ] Caching Optimization
- [ ] House Cleaning (`requirements.txt`...)
- [ ] AoE2 Resources (build orders and other sites)
- [ ] READEME.md as view

### Long Shots
- [ ] Messaging Board
- [ ] Discord Integration
- [x] Reddit Integration
- [ ] Twitch Integration
- [ ] Tournament Calendar
- [ ] `Docker` deployment
